package tpjava;

import java.util.TreeMap;
import java.util.Scanner;

public class Exercice1 {
	
	private static TreeMap<Integer, String> romansnumbers = new TreeMap<Integer, String>();
	
	// j'ai choisis le treemap comme type de collection car il fait le sort du cles d'une maniere naturels
	// ctd en ordre numerique dans notre cas
	// j'ai choisis d'afficher les nombre de 1 jusqua 59
	static {
        romansnumbers.put(50, "L");
        romansnumbers.put(40, "XL");
        romansnumbers.put(10, "X");
        romansnumbers.put(9, "IX");
        romansnumbers.put(5, "V");
        romansnumbers.put(4, "IV");
        romansnumbers.put(1, "I");	
	}
	
	//versnombreroman est une methode recursive qui retourne les nombres en romanies 
	// selon combien l'utilisateur veut entre 1 et 50
	public final static String versnombreroman(int cle)
	{
		//floorkey est une methode qui retourne le plus grand cle dans notre treemap inferieure ou egale la cle en parametre
		int l = romansnumbers.floorKey(cle);
		if (l == cle)
		{
			return romansnumbers.get(cle);
		}
		else {
		
		return romansnumbers.get(l) + versnombreroman(cle - l );
		}
	}
	
	public static void main(String args[]) {
		
		// tester notre programme
		/*
		for (int i = 1; i <= 50 ; i++) {
			System.out.println(i+"\t =\t "+versnombreroman(i));
		}
		*/
		
		// question 2
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		int tentatives = 2 ;
		int choix ;
		System.out.println("entrer un nombre entre 1 et 50");
		choix = sc.nextInt();
		while((choix>50 || choix < 1) && tentatives > 0)
		{
			System.out.println("ressayer a nouveau , reste : "+tentatives+" tentatives");
			choix = sc.nextInt();
			tentatives--;
		}
		
		if (choix>50 || choix < 1)
		{
			return ;
		}
		else {
			for (int i = 1; i <= choix ; i++) {
				System.out.println(i+"\t =\t "+versnombreroman(i));
			}
		}
		
	}
}
