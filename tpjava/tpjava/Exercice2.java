package tpjava;

import java.util.Scanner;

class Devinette {
	private int random_number;
	private int random_inf ;
	private int random_sup ;
	
	private Devinette(int borne1sup , int borne2sup , int borne1inf , int borne2inf)
	{
		this.random_inf = (int) (Math.random() * (borne2inf - borne1inf + 1) + borne1inf) ;
		this.random_sup = (int) (Math.random() * (borne2sup - borne1sup + 1) + borne1sup) ;
		this.random_number = (int) (Math.random() * (random_sup - random_inf + 1) + random_inf);
	}
	private void soumettreCoup() {
		int coups = 1 ;
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		System.out.print("Coup : ("+random_inf+"-"+random_sup+") : ");
		int tentative = sc.nextInt() ;
		while(tentative!=random_number)
		{
			if(tentative > random_number)
			{
				System.out.println("Plus Bas ... Coup : ("+random_inf+"-"+random_sup+") : "+tentative);
				tentative = sc.nextInt() ;
			}
			else {
				System.out.println("Plus Haut ... Coup : ("+random_inf+"-"+random_sup+") : "+tentative);
				tentative = sc.nextInt() ;
			}
			coups++;
		}
		System.out.println("Bravo, en "+coups+" coups.");
	}
	
	static void jouerpartie(int borne1sup , int borne2sup , int borne1inf , int borne2inf)
	{
		Devinette d = new Devinette(borne1sup, borne2sup, borne1inf, borne2inf);
		d.soumettreCoup();
	}
	
}
public class Exercice2 {
	
	public static void main(String args[]) {	
		int borne1sup = 40 ;
		int borne2sup = 50 ;
		int borne1inf = 10 ;
		int borne2inf = 20 ;
		for (int i = 1; i <=3; i++) {
		Devinette.jouerpartie(borne1sup,borne2sup,borne1inf,borne2inf);
		}
	}

}
