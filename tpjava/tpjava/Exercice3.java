package tpjava;
import java.util.*; 

class Skywalker {
	
	private String nom ;
	private String prenom ;
	private String sexe ;
	private int descendantDe;
	private String conjointDe ;
	public static int nombreDePersonnages = 0 ;
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	private Skywalker() {};
	
	private Skywalker(String nom, String prenom, String sexe) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.sexe = sexe;
		this.descendantDe = 0 ;
		nombreDePersonnages++;
	}
	
	private Skywalker(Skywalker P)
	{
		this.nom = P.nom;
		this.prenom = P.nom;
		this.sexe = P.sexe;
		this.descendantDe = P.descendantDe ;
	}

	@Override
	public String toString() {
		return "Skywalker [nom=" + nom + ", prenom=" + prenom + ", sexe=" + sexe + ", descendantDe=" + descendantDe
				+ ", conjointDe=" + conjointDe + "]";
	}

	static Skywalker ajouterSkywalker(String nom, String prenom, String sexe)
	{
		Skywalker p = new Skywalker(nom,prenom,sexe);
		return p ;
	}

	public void filsde(Skywalker p) {
		
		this.descendantDe = System.identityHashCode(p);
	}

	public void conjointde(String epouse) {
		this.conjointDe = epouse ;
	}
}


public class Exercice3 {
	
	public static void main(String args[]) {
		
		Skywalker shmi = Skywalker.ajouterSkywalker("skywalker","shmi","homme");
		Skywalker anakinK = Skywalker.ajouterSkywalker("skywalker","anakin","homme");
		Skywalker luke = Skywalker.ajouterSkywalker("skywalker","luke","homme");
		Skywalker Ben = Skywalker.ajouterSkywalker("skywalker","ben","homme");
		Skywalker nat = Skywalker.ajouterSkywalker("skywalker","nat","homme");
		Skywalker kol = Skywalker.ajouterSkywalker("skywalker","kol","homme");
		Skywalker code = Skywalker.ajouterSkywalker("skywalker","code","homme");
		
		shmi.filsde(null);
		shmi.conjointde(null);
		anakinK.filsde(shmi);
		anakinK.conjointde("padme amidala");
		luke.filsde(anakinK);
		luke.conjointde("mara jade");
		Ben.filsde(luke);
		Ben.conjointde(null);
		nat.filsde(Ben);
		nat.conjointde(null);
		kol.filsde(Ben);
		kol.conjointde("Morrigan Corde");
		code.filsde(kol);
		code.conjointde(null);
		
		List<Skywalker> list = new ArrayList<Skywalker>(); 
		list.add(shmi);
		list.add(anakinK);
		list.add(nat);
		list.add(kol);
		list.add(luke);
		list.add(Ben);
		list.add(code);
		
		list.stream().sorted((object1, object2) -> object1.getPrenom().compareTo(object2.getPrenom()));
		
		for(int i=0;i<list.size();i++){
		    System.out.println(list.get(i));
		}
		
		/*
		System.out.println("enregistrement : "+shmi.hashCode()+" "+shmi.toString());
		System.out.println("enregistrement : "+anakinK.hashCode()+" "+anakinK.toString());
		System.out.println("enregistrement : "+luke.hashCode()+" "+luke.toString());
		System.out.println("enregistrement : "+Ben.hashCode()+" "+Ben.toString());
		System.out.println("enregistrement : "+nat.hashCode()+" "+nat.toString());
		System.out.println("enregistrement : "+kol.hashCode()+" "+kol.toString());
		System.out.println("enregistrement : "+code.hashCode()+" "+code.toString());
		*/
		System.out.println("nombre de personnages : "+Skywalker.nombreDePersonnages);
	}

}
